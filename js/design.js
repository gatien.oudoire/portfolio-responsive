const navSlide = () => {
    const menu = document.querySelector('.menu-barres');
    const nav = document.querySelector('.nav-liens');
    const navLinks = document.querySelectorAll('.nav-liens li');
    const img_cercle = document.getElementById('img_cercle');

    menu.addEventListener('click', () => {
        nav.classList.toggle('nav-actif');
        if (getComputedStyle(img_cercle).display != "none"){
        img_cercle.style.display = "none";    // comme .img_cercle {opacity = 0;}
        }
        else 
        {
        img_cercle.style.display = "inline";
        console.log("y a un truc louche")
        }
    });

    navLinks.forEach((link, index) => {
        link.style.animation = `navLinkFade 0.5s ease forwards ${index / 7}s`
    }); 
}

navSlide();


