<!DOCTYPE html>
<html>
    <head>
        <title>Gatien Oudoire - Connexion</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel='stylesheet' href="css/connexion.css">
        <link rel="icon" href="img\logo-siteweb.png">
        <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@500&display=swap" rel="stylesheet">
    </head>

    <body>

        <nav>
            <br>
            <div class="logo">
                <h1>Gatien Oudoire - Connexion</h1>
            </div>
            <br>
        </nav>

        <br>
        <br>
        <br>
        <br>
        <br>

        <main>
            <form action="" method="POST">
                <center>
                <div class="div-connexion">
                    <br>
                    <label>Nom d'utilisateur :</label>
                    <br>
                    <br>
                    <input type="text" class="text-entree" name="username" id="username" placeholder="Entrez votre nom">
                    <br>
                    <br>
                    <label>Mot de passe :</label>
                    <br>
                    <br>
                    <input type="password" class="text-entree" name="password" id="password" placeholder="Entrez votre mot de passe">
                    <br>
                    <br>
                    <input type="submit" class="boutton-connexion" name="valider" value="Valider">
                    <br>
                    <br>
                </div>
                </center>
            </form>
        </main>

        <footer id="footer">
            <br>
            <center>
                <p class="bas">Nous utilisons des cookies uniquement pour sauvergarder vos informations de connexion</p>
            </center>
            <center>
                <input type="button" value="Accepter" onclick="validerCookie()">
            </center>
            <br>
        </footer>
        <script src="js/design-connexion.js"></script>
    </body>
</html>

<?php

$host = "localhost";
$user = "root";
$password = "";
$db = "utilisateurs";

?>


